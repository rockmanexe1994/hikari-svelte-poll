import { writable } from 'svelte/store'

export const pollList = writable(
    [
        {
            id: (new Date).getTime(),
            question: "Gun Gale Online or ALfheim Online ?",
            option_1: "Gun Gale Online",
            option_2: "ALfheim Online",
            vote_1: 0,
            vote_2: 0,
        },
    ]
)